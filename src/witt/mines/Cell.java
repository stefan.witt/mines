package witt.mines;

import static witt.mines.CellState.*;


public class Cell {

    private boolean bomb;
    private CellState state;

    public Cell() {
        bomb = false;
        state = HIDDEN;
    }

    public int setBomb() {
        int result = bomb ? 0 : 1;
        bomb = true;
        return result;
    }

    public boolean isBomb() {
        return bomb;
    }

    public int getBombCount() {
        if (bomb)
            return 1;
        else
            return 0;
    }

    public boolean mark() {
        boolean result = state == HIDDEN;
        state = MARKED;
        return result;
    }

    public boolean probe() {
        state = TESTED;
        if (bomb) throw new Bomb();
        return false;
    }

    public int countUnfinished() {
        return state == TESTED || (state == MARKED && bomb) ? 0 : 1;
    }


}

package witt.mines.witt.util;

public interface UpdateListener {
    void update();
}

package witt.mines;

import witt.mines.witt.util.AbstractUpdater;
import witt.mines.witt.util.Logging;

import java.util.Random;

public class Field extends AbstractUpdater {

    private int cols, rows;
    private Random random;
    private Cell[][] cell;
    private boolean finished;

    public Field(int size, int mines) {
        super();
        finished = false;
        random = new Random();
        cols = size;
        rows = size;
        initCells();
        hideMines(mines);
    }

    public Field() {this(10, 10);}

    private void initCells() {
        cell = new Cell[rows][cols];
        for (int r=0; r<rows; r++)
            for (int c=0; c<cols; c++)
                cell[r][c] = new Cell();
    }

    private void hideMines(int n) {
        if (n <= 0) return;
        int c = random.nextInt(cols);
        int r = random.nextInt(rows);
        int delta = cell[r][c].setBomb();
        hideMines(n-delta);
    }

    public boolean mark(int r, int c) {
        if (!inBounds(r,c)) return false;
        boolean mark = cell[r][c].mark();
        testFinish();
        return mark;
    }

    public int probe(int r, int c) {
        if (!inBounds(r,c)) return 0;
        cell[r][c].probe();
        testFinish();
        return countBombs(r, c);
    }

    private boolean inBounds(int r, int c) {
        return r>=0 && r<rows && c>=0 && c<cols;
    }

    private int countBombs(int xr, int xc) {
        int sum = 0;
        for (int r = xr-1; r<=xr+1; r++)
            for (int c = xc-1; c<=xc+1; c++)
                sum += inBounds(r, c) ? cell[r][c].getBombCount() : 0;
            return sum;
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return cols;
    }

    public boolean isFinished() { return finished; }

    public void testFinish() { new FinishTester().start(); }

    public void setFinished(boolean f) { finished = f; }

    public class FinishTester extends Thread {
        public void run() {
            int remaining = 0;
            for (int r = 0; r < rows; r++)
                for (int c = 0; c < cols; c++)
                    remaining +=cell[r][c].countUnfinished();
            finished = remaining == 0;
            // log("remaining cells: " + remaining);
            update();
        }
    }

}




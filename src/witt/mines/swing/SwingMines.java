package witt.mines.swing;

import witt.mines.Bomb;
import witt.mines.Field;
import witt.mines.witt.util.Logging;
import witt.mines.witt.util.UpdateListener;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static java.awt.Color.*;
import static java.awt.event.ActionEvent.SHIFT_MASK;

public class SwingMines extends JFrame implements UpdateListener{

    private final static int BOMB = -1;
    private final static int MARK = -2;

    private Field field;

    public SwingMines(Field f) {
        field = f;
        field.addUpdateListener(this);
        initComponents();
        initWindow();
    }

    private void initComponents(){
        this.setLayout(new GridLayout(field.getRows()+1, field.getColumns()+1));
        for (int c=0; c<field.getColumns()+1; c++)
            add(makeLabel(c),0, c);
        for (int r=0; r<field.getRows(); r++) {
            add(makeLabel(r+1), r+1, 0);
            for (int c=0; c<field.getColumns(); c++)
                add(makeButton(r, c),r+1, c+1);
        }
    }


    private Component makeLabel(int c) {
        String text = c <=0 ? "" : Integer.toString(c);
        JLabel label = new JLabel(text);
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setVerticalAlignment(JLabel.CENTER);
        return label;
    }

    private Component makeButton(int r, int c) {
        JButton button = new JButton("?");
        button.setForeground(Color.GRAY);
        button.setPreferredSize(new Dimension(40,40));
        button.addActionListener(new Probe(r, c, button));
        return button;
    }

    public void initWindow() {
        this.setSize(this.getPreferredSize());
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }


    public void update() {
        // log("Update triggered - finished: " + field.isFinished());
        if (field.isFinished())
            success();
    }

    public void success() {
        JOptionPane.showMessageDialog(this,
                "Du hast gewonnen!");
    }



    public static void main(String[] args) {
        try {
            new SwingMines(new Field());
        } catch (Bomb b) {
            JOptionPane.showMessageDialog(null,
                    "Du hast verloren!");

        }
    }

    class Probe implements ActionListener {
        private int br, bc;
        private JButton button;

        public Probe(int r, int c, JButton b) {
            br = r; bc = c;
            button = b;
        }

        public void actionPerformed(ActionEvent e) {
            boolean shift = (e.getModifiers() & SHIFT_MASK) != 0;
            if (shift) {
                field.mark(br,bc);
                display(MARK);
            }
            else {
                try {
                    int result = field.probe(br, bc);
                    display(result);
                } catch (Bomb b){
                    display(BOMB);
                    field.setFinished(true);
                    JOptionPane.showMessageDialog(SwingMines.this,
                            "Du hast verloren!");

                }
            }
        }

        private void display(int state) { ;
            button.setForeground(color(state));
            button.setText(text(state));
        }

        private Color color(int state) {
            return state<0 || state >2 ? RED : BLACK;
        }

        private String text(int state) {
            if (state>0)
                return Integer.toString(state);
            switch(state) {
                case BOMB:
                    return "*";
                case MARK:
                    return "X";
                default:
                    return "";
            }
        }

    }



}

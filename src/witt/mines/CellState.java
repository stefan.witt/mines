package witt.mines;

public enum CellState {
    HIDDEN,
    MARKED,
    TESTED;
}

package witt.mines.witt.util;

public interface Logging {
    default void log(String msg) {
        System.out.println(msg);
    }
}

package witt.mines.witt.util;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractUpdater {

    private List<UpdateListener> listeners;

    public AbstractUpdater() {
        listeners = new ArrayList<UpdateListener>();
    }

    public void addUpdateListener(UpdateListener listener) {
        listeners.add(listener);
    }

    public void update() {
        for (UpdateListener listener : listeners)
            listener.update();
    }


}
